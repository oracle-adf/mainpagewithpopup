package view;

import javax.faces.event.ActionEvent;

import model.EmpVORowImpl;

import oracle.adf.model.BindingContext;
import oracle.adf.model.binding.DCBindingContainer;
import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.share.ADFContext;

import org.apache.myfaces.trinidad.context.RequestContext;

public class DepartmentId {
    public DepartmentId() {
    }

    public void findDeptId(ActionEvent actionEvent) {
        RequestContext requestContext = RequestContext.getCurrentInstance();

        requestContext.getPageFlowScope().put("pDeptId", getDepartmentID());
        System.out.println("Department id:" + requestContext.getPageFlowScope().get("pDeptId"));

    }


    public Integer getValue() {
        Integer id = (Integer) ADFContext.getCurrent().getPageFlowScope().get("pDeptId");
        System.out.println("Dept id:" + id);
        return id;
    }

    public Integer getDepartmentID() {
        return (Integer) getEmpVORowImpl().getAttribute("DepartmentId");
    }

    private EmpVORowImpl getEmpVORowImpl() {
        DCBindingContainer dcBindings = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
        DCIteratorBinding iterBind = (DCIteratorBinding) dcBindings.get("EmpVO1Iterator");
        EmpVORowImpl row = (EmpVORowImpl) iterBind.getCurrentRow();
        return row;
    }
}
